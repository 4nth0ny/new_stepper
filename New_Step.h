//by Anthony Kozlov
//With love
//from mother Russia
//2017
//kozlanton182@gmail.com

#include <Arduino.h>

class New_Step
{
  public:
    New_Step(int in1, int in2, int in3, int in4);
    ~New_Step();

    void set_speed(int spd);

    void stop();

    int SPEED;
    int IN1;
    int IN2;
    int IN3;
    int IN4;


    void rotate_360(bool side);

};

